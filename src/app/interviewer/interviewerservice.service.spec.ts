import { TestBed, inject } from '@angular/core/testing';

import { InterviewerserviceService } from './interviewerservice.service';

describe('InterviewerserviceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InterviewerserviceService]
    });
  });

  it('should be created', inject([InterviewerserviceService], (service: InterviewerserviceService) => {
    expect(service).toBeTruthy();
  }));
});
