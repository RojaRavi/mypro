import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InterviewerserviceService } from '../../interviewerservice.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers:[InterviewerserviceService]
})
export class DashboardComponent implements OnInit {
event;
  constructor(private routers:Router,public iservice:InterviewerserviceService) {
    this.iservice.fetchData();
    this.event=this.iservice.data;
    //console.log(this.event);
   }
   redirect():void{
 this.routers.navigate(['/event-enrollment']);
   }

  ngOnInit() {
  }

}
