import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ValidationErrors } from '@angular/forms';
import { ServiceregisterService } from "../../serviceregister.service";
// import {Skill} from '../../../commonislot/classes/Skill';
import {UserModel} from '../../../commonislot/classes/user';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  idformgroup:FormGroup;

  constructor(private service:ServiceregisterService) { 
    this.idformgroup=new FormGroup({
      mailid:new FormControl('',[Validators.required,Validators.pattern(/^([A-Za-z0-9_\-\.])+\@([virtusa|VIRTUSA])+\.(com)$/)]),
      name:new FormControl(''),
      password:new FormControl(''),
      phonenumber:new FormControl(''),
      location:new FormControl(''),
      employeeid:new FormControl('')
    })
  }
  onSave()
  {
      
  
         var name=this.idformgroup.get('name').value;
         var mailid=this.idformgroup.get('mailid').value;
         var phonenumber=this.idformgroup.get('phonenumber').value;
         var password=this.idformgroup.get('password').value;
         var location=this.idformgroup.get('location').value;
         var employeeid=this.idformgroup.get('employeeid').value;
      // var userobject=new User( 10,"ram","abc@gmail.com",999999999,"admin","chennai",[]);  
    
       const userModel:UserModel = new UserModel(10,"ram","abc@gmail.com",999999999,"admin","chennai",[],"sdd");
       console.log(userModel);
      
    console.log(JSON.stringify(userModel))
    this.service.newuser(JSON.stringify(userModel))
    .subscribe(
          (Response)=>console.log(Response),
          (Error)=>console.log(Error)
    )

  }
  ngOnInit() {
    
  }

}
