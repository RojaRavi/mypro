import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup,AbstractControl,ValidationErrors,Validators } from '@angular/forms';
import { Http } from '@angular/http';
@Component({
  selector: 'app-event-creation',
  templateUrl: './event-creation.component.html',
  styleUrls: ['./event-creation.component.css']
})
export class EventCreationComponent implements OnInit {
  regFormGroup: FormGroup;
  errMessage:string;

  constructor(private httpService: Http) {
    this.regFormGroup = new FormGroup({
      firstName: new FormControl('', Validators.required),
      location: new FormControl('chennai', Validators.required),
      skill: new FormControl('Angular', Validators.required),
    })
  }

  ngOnInit() {
  }
  check(date1 : Date)
  { 
    this.errMessage="";
    var cur=new Date();
    if(new Date(date1) < cur)
      {
        this.errMessage="Invalid Date";
        return false;
      }
  }
  onCreateEvent() {
    console.log(this.regFormGroup.value);

    const postData = {

    }
    const url = 'https://islot-34ffe.firebaseio.com/events.json';
    this.httpService.post(url, this.regFormGroup.value)
      .subscribe(rsp => console.log(rsp));

    console.log('Posted');
  }


}



