export class Events {
    name:string;
    date:string;
    startTime:string;
    endTime:string;
    createdBy:number;
    locatioin:string;
    skill:Array<string>;
    idClosed:boolean;
    splitTime:Array<String>;
    slots:Array<{slotStarTime:String,slotEndTime:String,interviewers:Array<{id:number,name:string,mail:string,mobileNo:number,noOfInterviews:number}>}>;
    constructor(name:string,date:string,startTime:string,endTime:string,createdBy:number,locatioin:string,skill:Array<string>){
             this.name=name;
             this.date=date;
             this.startTime=startTime;
             this.endTime=endTime;
             this.createdBy=createdBy;
             this.locatioin=locatioin;
             this.skill=skill;
             this.idClosed=false;
             this.splitTime=this.timeArray(startTime,endTime);
             this.slots=[{
                 slotStarTime:this.splitTime[0],
                 slotEndTime:this.splitTime[1],
                 interviewers:[{id:null,name:null,mail:null,mobileNo:null,noOfInterviews:null}]
             },
             {
                slotStarTime:this.splitTime[1],
                slotEndTime:this.splitTime[2],
                interviewers:[{id:null,name:null,mail:null,mobileNo:null,noOfInterviews:null}]
            },
            {
                slotStarTime:this.splitTime[2],
                slotEndTime:this.splitTime[3],
                interviewers:[{id:null,name:null,mail:null,mobileNo:null,noOfInterviews:null}]
            },
            {
                slotStarTime:this.splitTime[3],
                slotEndTime:this.splitTime[4],
                interviewers:[{id:null,name:null,mail:null,mobileNo:null,noOfInterviews:null}]
            },
            ]

        
        }
         timeArray(start, end){
            var start = start.split(":");
            var end = end.split(":");
        
            start = parseInt(start[0]) * 60 + parseInt(start[1]);
            end = parseInt(end[0]) * 60 + parseInt(end[1]);
            var temp= (end-start)/4;
            var result = [];
        
            for (var time = start; time <= end; time+=temp){
                result.push( this.timeString(time));
            }
            
            return result;
        }
        
         timeString(time){
            var hours = String(Math.floor(time / 60));
            var minutes = String(time % 60);
        
            if (hours < "10") hours = "0" + hours; //optional
            if (minutes < "10") minutes = "0" + minutes;
        
            return hours + ":" + minutes;
        }

}
