import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes } from '@angular/router';
import { UpdatedetailsComponent } from './pages/updatedetails/updatedetails.component';
import { LoginComponent } from '../registration/pages/login/login.component';
import { ChangeComponent } from './pages/change/change.component';
export const rout:Routes=[
  {path:"update",component:UpdatedetailsComponent},
  {path:"logout",component:LoginComponent},
  {path:"change",component:ChangeComponent}
]

@NgModule({
  imports: [
    CommonModule
  ],
  exports:[
    rout
  ],
  declarations: []
})
export class CommonRoutingModule { }
