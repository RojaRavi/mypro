import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mainheader',
  templateUrl: './mainheader.component.html',
  styleUrls: ['./mainheader.component.css']
})
export class MainheaderComponent implements OnInit {

  constructor(private routers:Router) { }
  changepass():void{
    this.routers.navigate(['/change']);
      }
      update():void{
        this.routers.navigate(['/update']);
          }
          log():void{
            this.routers.navigate(['/logout']);
              }
           
   
  ngOnInit() {
  }

}
